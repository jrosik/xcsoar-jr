CHARSET = utf-8

ifeq ($(CLANG)$(HOST_IS_DARWIN),nn)
CXX_FEATURES += -finput-charset=$(CHARSET)
C_FEATURES += -finput-charset=$(CHARSET)
endif
